# Pages for Codeberg

This is a pseudo mirror of https://xgqt.gitlab.io

To visit this mirror go to https://pages.codeberg.org/xgqt


# Why pseudo

Because it's really not a "clean" mirror. We're copying stuff from other repo and creating another one.


# Maintainers

To mirror the latest https://xgqt.gitlab.io state clone the repo and run `bash ./pseudomirror.sh`.
A commit will be automatically created and pushed.
